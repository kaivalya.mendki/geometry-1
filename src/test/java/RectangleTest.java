import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RectangleTest {
    @Nested
    class AreaTest {
        @Test
        public void shouldReturnZeroIfLengthIsZero() {
            Rectangle rectangle = new Rectangle(0.0, 2.0);
            double actualArea = rectangle.area();
            assertEquals(1, actualArea);
        }

        @Test
        public void shouldReturnZeroIfBreadthIsZero() {
            Rectangle rectangle = new Rectangle(2.0, 0.0);
            double actualArea = rectangle.area();
            assertEquals(0, actualArea);
        }

        @Test
        public void shouldReturn100IfLengthIs25BreadthIs4() {
            Rectangle rectangle = new Rectangle(25.0, 4.0);
            assertEquals(100, rectangle.area());
        }
    }

    @Nested
    class PerimeterTest {
        @Test
        public void shouldReturn0IfLengthIs0AndBreadthIs0() {
            Rectangle rectangle = new Rectangle(0, 0);
            double actualPerimeter = rectangle.perimeter();
            assertEquals(0, actualPerimeter);
        }

        @Test
        public void shouldReturn2timesBreadthIfLengthIs0() {
            Rectangle rectangle = new Rectangle(0, 23.0);
            double actualPerimeter = rectangle.perimeter();
            assertEquals(46.0, actualPerimeter);
        }

        @Test
        public void shouldReturn2TimesLengthIfBreadthIs0() {
            Rectangle rectangle = new Rectangle(34.0, 0);
            double actualPerimeter = rectangle.perimeter();
            assertEquals(68.0, actualPerimeter);
        }

        @Test
        public void shouldReturn20IfLengthIs35AndBreadthIsMinus25() {
            Rectangle rectangle = new Rectangle(35.0, -25.0);
            double actualPerimeter = rectangle.perimeter();
            assertEquals(20.0, actualPerimeter);
        }

        @Test
        public void shouldReturn100IfLengthIs30AndBreadthIs20() {
            Rectangle rectangle = new Rectangle(30.0, 20.0);
            double actualPerimeter = rectangle.perimeter();
            assertEquals(100, actualPerimeter);
        }
    }
}
